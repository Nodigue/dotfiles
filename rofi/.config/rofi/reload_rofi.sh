#!/bin/bash

# Symlink rofi config
ln -sf ${XDG_CACHE_HOME}/wal/rofi_theme.rasi ${XDG_CONFIG_HOME}/rofi/rofi_theme.rasi
ln -sf ${XDG_CACHE_HOME}/wal/rofi_power_menu.rasi ${XDG_CONFIG_HOME}/rofi/rofi_power_menu.rasi
