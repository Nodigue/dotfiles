#!/bin/bash

# Rofi
if [ -e ${XDG_CONFIG_HOME}/rofi/reload_rofi.sh ]; then
    ${XDG_CONFIG_HOME}/rofi/reload_rofi.sh
fi

# Dunst
if [ -e ${HOME}/.config/dunst/reload_dunst.sh ]; then
    ${HOME}/.config/dunst/reload_dunst.sh
fi
