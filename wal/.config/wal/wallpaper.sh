#!/bin/bash

function get_wallpapers()
{
    # $1 : Wallpapers directory

    wallpapers=()

    for file in "${1}"/*; do
        wallpapers+=($file)
    done
}

function write_index()
{
    # $1 : Index to write
    # $2 : Wallpaper index file

    echo "${1}" > ${2}
}

function get_current_index()
{
    # $1 : Wallpaper index file
    # $2 : Wallpaper list length

    if [[ -f ${1} ]]; then
        current_index=`cat ${1}`

        if (( ${current_index} < 0 )); then
            (( current_index=0 ))

        elif (( ${current_index} > ${2} - 1 )); then
            (( current_index=${2}-1 ))
        fi

    else
        write_index 0 ${1}
    fi
}

function get_next_index()
{
    # $1 : Current wallpaper index
    # $2 : Wallpaper list length

    next_index=${1}

    if (( ${1} < ${2} - 1)); then
        (( next_index=${1}+1 ))
    else
        (( next_index=0 ))
    fi
}

function get_previous_index()
{
    # $1 : Current wallpaper index
    # $2 : Wallpaper list length

    previous_index=${1}

    if (( ${1} > 1)); then
        (( previous_index=${1}-1 ))
    else
        (( previous_index=${2}-1 ))
    fi
}

function set_wallpaper()
{
    # $1 : Wallpaper path

    wal -i ${1} -o ${XDG_CONFIG_HOME}/wal/reload_templates.sh
    notify-send -u normal  "${notification_header}" "Setting wallpaper" -i ${1}
}

function load()
{
    get_wallpapers ${wallpapers_directory}
    get_current_index ${wallpaper_index_file} ${#wallpapers[@]}

    current_wallpaper=${wallpapers[current_index]}
}

function remove_wallpaper()
{
    rm -rf `readlink -f ${current_wallpaper}`
    rm -rf ${current_wallpaper}
}

wallpapers_directory="${HOME}/docs/images/wallpapers/upscaled"
wallpaper_index_file="${XDG_CACHE_HOME}/wal/wal_index"

notification_header="🖥 Wallpaper"

load

case "$1" in
    --next)

        get_next_index ${current_index} ${#wallpapers[@]}
        set_wallpaper ${wallpapers[${next_index}]}
        write_index ${next_index} ${wallpaper_index_file}
        ;;

    --previous)

        get_previous_index ${current_index} ${#wallpapers[@]}
        set_wallpaper ${wallpapers[${previous_index}]}
        write_index ${previous_index} ${wallpaper_index_file}
        ;;

    --dislike)

        remove_wallpaper
        load
        set_wallpaper ${current_wallpaper}
        ;;

    *)
        set_wallpaper ${current_wallpaper}
        ;;

esac
