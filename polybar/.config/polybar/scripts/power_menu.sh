#!/bin/bash

declare -a texts
texts[0]=" Shutdown"
texts[1]="勒 Reboot"
texts[2]=" Lock"
texts[3]=" Suspend"

declare -a actions
actions[0]="systemctl poweroff"
actions[1]="systemctl reboot"
actions[2]="loginctl lock-session ${XDG_SESSION_ID-}"
actions[3]="systemctl suspend"

options="${texts[0]}"

for text in "${texts[@]:1}"; do
  options+="\n${text}"
done

result=$(echo -e ${options} | rofi -theme ${XDG_CONFIG_HOME}/rofi/rofi_power_menu.rasi -dmenu)

index=-1
for (( i=0; i<4; i++ )); do 

  if [[ "$result" == "${texts[$i]}" ]]; then
    index=$i
  fi

done

if (( index >= 0 && index <= 3)); then
  eval ${actions[$index]}
fi
