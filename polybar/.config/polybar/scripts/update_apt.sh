#!/bin/sh

updates=$(apt list --upgradable 2> /dev/null | grep -c "mis à jour");

if [ "$updates" -gt 0 ]; then
    echo "$updates"
else
    echo "0"
fi