#!/bin/bash

layout_list=("fr" "us" "de")
flag_list=(🇫🇷 🇺🇸 🇩🇪)

current_layout=`setxkbmap -print | awk -F"+" '/xkb_symbols/ {print $2}'`

function get_layout_output() {
    layout_output="${layout_list[$1]^^} ${flag_list[$1]^^}"
}

function get_layout_index() {
    layout_index=0
	for layout in "${layout_list[@]}"; do
        if [[ $layout == "$1" ]]; then
            break;
        fi
        (( layout_index++ ))
    done
}

function get_next_layout_index() {
    get_layout_index $1

    if (( layout_index < ${#layout_list[@]} - 1)); then
        (( layout_index=layout_index+1 ))
    else
        (( layout_index=0 ))
    fi
}

case "$1" in
    --switch)
        get_next_layout_index $current_layout
        get_layout_output $layout_index
        notify-send -u normal "⚙️ Settings" "New keyboard layout : ${layout_output}"
        setxkbmap ${layout_list[$layout_index]}
        ;;

    *)
        get_layout_index $current_layout
        get_layout_output $layout_index
        echo $layout_output
        ;;
esac
