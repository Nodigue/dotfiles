#!/bin/bash

function fetch_memory_usage() {

    # free - Display the amount of free and used memory in the system
    read -a result <<< $(free | grep "Mem:")
   
    total=${result[1]}
    used=${result[2]}
}

fetch_memory_usage

case "$1" in
    --popup)
        notify-send -u normal "🖥 System - Memory (%)" "$(ps axch -o cmd:10,pmem k -pmem | head | awk '$0=$0"%"' )"
        ;;
    *)
        percentage_used=$(awk -v u=$used -v t=$total 'BEGIN {printf("%.1f\n", (u/t)*100)}')
        echo "$percentage_used%"
        ;;
esac
