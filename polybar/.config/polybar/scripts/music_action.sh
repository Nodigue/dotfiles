#!/bin/sh

if ! mpc >/dev/null 2>&1; then
  exit 1

elif mpc status | grep -q playing; then
  mpc pause
else
  mpc play
fi

