#!/bin/sh

music_symbol=" "
pause_symbol=" "


if ! mpc >/dev/null 2>&1; then
  exit 1

elif mpc status | grep -q playing; then
  ( mpc current | zscroll -b "${music_symbol} " -l 30 -d 0.3 ) &

else
  echo "${music_symbol} ${pause_symbol}"
fi

# Block until an event is emitted
mpc idle >/dev/null