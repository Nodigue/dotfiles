#!/bin/bash

PATH_POWER_SUPPLY="/sys/class/power_supply/"

# Empty folder : not a laptop
if [[ -z "$(ls -A ${PATH_POWER_SUPPLY})" ]]; then
    echo "ﳥ"
    return
fi

BATTERY_LOW_THRESHOLD=10
BATTERY_FULL_THRESHOLD=97

PATH_ADP="/sys/class/power_supply/AC"
PATH_BATTERY="/sys/class/power_supply/BAT1"

CACHE_DIR="${XDG_CACHE_HOME}/polybar/battery/"
CACHE_FILE_CHARGE="${CACHE_DIR}/last_charging"
CACHE_FILE_BATTERY_LEVEL="${CACHE_DIR}/last_battery_level"

# --------------------------------------------------------------------------------- #
# SCRIPT
# --------------------------------------------------------------------------------- #

# Get current state

current_charging=0
current_battery_level=0
battery_max=1

if [[ -f "${PATH_ADP}/online" ]]; then
    current_charging=$(cat "$PATH_ADP/online")
fi

if [[ -f "${PATH_BATTERY}/energy_now" ]]; then
    current_battery_level=$(cat "$PATH_BATTERY/energy_now")
fi

if [[ -f "${PATH_BATTERY}/energy_full" ]]; then
    battery_max=$(cat "$PATH_BATTERY/energy_full")
fi

current_battery_percent=$(("$current_battery_level * 100"))
current_battery_percent=$(("$current_battery_percent / $battery_max"))

# Output current state

if [ "$current_charging" -eq 1 ]; then
    if [ "$current_battery_percent" -gt ${BATTERY_FULL_THRESHOLD} ]; then
        echo ""
    else
        echo "  $current_battery_percent %"
    fi
else
    if [ "$current_battery_percent" -gt 90 ]; then
        icon=""
    elif [ "$current_battery_percent" -gt 80 ]; then
        icon=""
    elif [ "$current_battery_percent" -gt 70 ]; then
        icon=""
    elif [ "$current_battery_percent" -gt 60 ]; then
        icon=""
    elif [ "$current_battery_percent" -gt 50 ]; then
        icon=""
    elif [ "$current_battery_percent" -gt 40 ]; then
        icon=""
    elif [ "$current_battery_percent" -gt 30 ]; then
        icon=""
    elif [ "$current_battery_percent" -gt 20 ]; then
        icon=""
    elif [ "$current_battery_percent" -gt 10 ]; then
        icon=""
    fi

    echo "$icon $current_battery_percent %"
fi

# Compare to last state

if [[ -f "${CACHE_FILE_CHARGE}" ]]; then
    last_charging=$(cat ${CACHE_FILE_CHARGE})

    if [ "${last_charging}" == 1 ] && [ "${current_charging}" == 0 ]; then
        notify-send -u normal "Power Management" "😱 Unplugged"
    elif [ "${last_charging}" == 0 ] && [ "${current_charging}" == 1 ]; then
        notify-send -u normal "Power Management" "🔌 Plugged"
    fi
else
    mkdir -p ${CACHE_DIR}
fi

if [[ -f "${CACHE_FILE_BATTERY_LEVEL}" ]]; then
    last_battery_level=$(cat ${CACHE_FILE_BATTERY_LEVEL})

    if [ "${last_battery_level}" >= ${BATTERY_LOW_THRESHOLD} ] && [ "${current_battery_level}" <= ${BATTERY_LOW_THRESHOLD} ]; then
        notify-send -u normal "Power Management" "😱 Battery : ${BATTERY_LOW_THRESHOLD}"
    fi

    if [ "${last_battery_level}" <= ${BATTERY_FULL_THRESHOLD} ] && [ "${current_battery_level}" >= ${BATTERY_FULL_THRESHOLD} ]; then
        notify-send -u normal "Power Management" "💪 Battery full"
    fi
else
    mkdir -p ${CACHE_DIR}
fi

# Save current state

echo "${current_charging}" > ${CACHE_FILE_CHARGE}
echo "${current_battery_level}" > ${CACHE_FILE_BATTERY_LEVEL}
