local km = vim.keymap.set

-- set "space" as leader key
vim.g.mapleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- save with Ctrl + s
km({ "n", "i" }, "<C-s>", "<esc><cmd>wa<CR>", { desc = "Save all opened buffers" })

-- TABS
-- km("n", "H", "gT", { desc = "a" })
-- km("n", "L", "gt", { desc = "a" })
-- km("n", "tt", ":tabnew<CR>", { desc = "a" })

-- Create splits
km("n", "<leader>hs", ":split<CR>", { desc = "Create a new horizontal split" })
km("n", "<leader>vs", ":vsplit<CR>", { desc = "Create a new vertical split" })

-- Navigate between split with Ctrl + {h, j, k, l}
km("n", "<C-h>", "<C-w>h", { desc = "Move to left buffer" })
km("n", "<C-j>", "<C-w>j", { desc = "Move to up buffer" })
km("n", "<C-k>", "<C-w>k", { desc = "Move to down buffer" })
km("n", "<C-l>", "<C-w>l", { desc = "Move to right buffer" })

km({ "n", "v" }, "L", "$", { desc = "Move cursor to the end of the line" })
km({ "n", "v" }, "H", "^", { desc = "Move cursor to the beginning of the line" })

-- Resize splits with Ctrl + arrows
km("n", "<C-Up>", ":resize -2<CR>", { desc = "Reduce horizontal buffer size" })
km("n", "<C-Down>", ":resize +2<CR>", { desc = "Increase horizontal buffer size" })
km("n", "<C-Left>", ":vertical resize -2<CR>", { desc = "Reduce vertical buffer size" })
km("n", "<C-Right>", ":vertical resize +2<CR>", { desc = "Increase vertical buffer size" })

-- Move text up and down
-- km("n", "<C-A-j>", ":m+1<CR>==", { desc = "a" })
-- km("i", "<C-A-j>", "<ESC>:m+1<CR>==gi", { desc = "a" })
-- km("v", "<C-A-j>", ":m'>+<CR>gv=gv", { desc = "a" })
-- km("n", "<C-A-k>", ":m-2<CR>==", { desc = "a" })
-- km("i", "<C-A-k>", "<ESC>:m-2<CR>==gi", { desc = "a" })
-- km("v", "<C-A-k>", ":m-2<CR>gv=gv", { desc = "a" })

-- Keep indenting with < and >
km("v", "<", "<gv", { desc = "Reduce selection indentation" })
km("v", ">", ">gv", { desc = "Increase selection indentation" })

-- -------------------------------------------------------------------------- --
-- TELESCOPE MAPPINGS
-- -------------------------------------------------------------------------- --
local function change_encoding(selected_encoding)
    local present, notify = pcall(require, "notify")

    vim.cmd("e ++enc=" .. selected_encoding)
    if present then
        notify("Changed encoding to: " .. selected_encoding)
    end
end

local encoding_picker = function()
    local encodings = {
        "utf-8",
        "utf-16",
        "latin1",
        "ascii",
        "iso-8859-1",
        "windows-1252",
    }

    local actions = require("telescope.actions")
    local action_state = require("telescope.actions.state")


    local themes = require("telescope.themes")
    local picker_theme = themes.get_ivy({
        layout_config = {
            height = #encodings + 3,
        },
    })

    require("telescope.pickers").new(picker_theme, {
        prompt_title = "Select file encoding",
        finder = require('telescope.finders').new_table({
            results = encodings,
        }),
        sorter = require("telescope.sorters").empty(),
        attach_mappings = function(_, map)
            map('i', '<CR>', function(prompt_bufnr)
                local selected = action_state.get_selected_entry()
                actions.close(prompt_bufnr)
                if selected then
                    change_encoding(selected[1])
                end
            end)
            return true
        end,
    }):find()
end

vim.keymap.set("n", "<leader>ec", function()
    encoding_picker()
end)
