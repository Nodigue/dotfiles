local nb_tab = 4

local options = {
    encoding = "utf-8",

    -- Time to wait for a mapped sequence to complete (in milliseconds)
    timeoutlen = 1000,

    relativenumber = true,
    number = true,

    errorbells = false,
    hlsearch = false,

    -- "How many of whitespace a \t char is worth?".
    tabstop = nb_tab,
    -- "How many of whitespace a level of indentation is worth?".
    shiftwidth = nb_tab,
    -- "How many of whitespace should be inserted when the 'tab' key is pressed?".
    softtabstop = nb_tab,
    -- Should we replace \t char by spaces.
    expandtab = true,

    -- Always show tabs.
    showtabline = 1,

    smartcase = true,
    smartindent = true,

    wrap = false,
    scrolloff = 8,
    swapfile = false,
    backup = false,
    writebackup = false,
    cursorline = true,

    colorcolumn = "80",

    splitbelow = true,
    splitright = true,

}

for key, value in pairs(options) do
    vim.opt[key] = value
end

