return {
    {
        'saghen/blink.cmp',
        dependencies = 'rafamadriz/friendly-snippets',
        version = 'v0.*',
        opts = {
            keymap = { preset = 'default' },
            appearance = {
                nerd_font_variant = 'mono',
            },
        },
    },
    {
        "williamboman/mason.nvim",
        config = function()
            require("mason").setup({
            })
        end
    },
    {
        "williamboman/mason-lspconfig.nvim",
        config = function()
            require("mason-lspconfig").setup({
                ensure_installed = {
                    "lua_ls",
                    "clangd",
                    "cmake",
                    "pyright",
                    "lemminx",
                },
                automatic_installation = true,
            })
        end
    },
    {
        "neovim/nvim-lspconfig",
        dependencies = {
            {
                "saghen/blink.nvim",
            },
            {
                "folke/lazydev.nvim",
                ft = "lua", -- only load on lua files
                opts = {
                    library = {
                        -- See the configuration section for more details
                        -- Load luvit types when the `vim.uv` word is found
                        { path = "${3rd}/luv/library", words = { "vim%.uv" } },
                    },
                },
            },
        },
        config = function()
            local lspconfig = require("lspconfig")
            local capabilities = require("blink.cmp").get_lsp_capabilities()

            -- LUA Support
            lspconfig.lua_ls.setup({
                capabilities = capabilities,
            })

            -- C++ Support
            lspconfig.clangd.setup({
                capabilities = capabilities,
            })

            -- CMake Support
            lspconfig.cmake.setup({
                capabilities = capabilities,
            })

            -- Python Support
            lspconfig.pyright.setup({
                capabilities = capabilities,
            })

            -- XML Support
            lspconfig.lemminx.setup({
                filetypes = { "xml", "urdf" },
                capabilities = capabilities,
            })

            vim.keymap.set("n", "<leader>gd", vim.lsp.buf.definition, { desc = "Go to definition" })
            vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, { desc = "Code action" })

            vim.api.nvim_create_autocmd("LspAttach", {
                callback = function(args)
                    local client = vim.lsp.get_client_by_id(args.data.client_id)
                    if not client then return end

                    -- Format the current buffer on save
                    if client.supports_method('textDocument/formatting', 0) then
                        vim.api.nvim_create_autocmd('BufWritePre', {
                            callback = function()
                                vim.lsp.buf.format({ bufnr = args.buf, id = client.id })
                            end,
                        })
                    end
                end
            })
        end
    }
}
