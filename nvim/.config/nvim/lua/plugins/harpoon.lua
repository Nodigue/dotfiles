return {
    'ThePrimeagen/harpoon',
    dependencies = {
        'nvim-lua/plenary.nvim',
    },
    config = function()
        local add_file = function()
            local harpoon = require("harpoon.mark")
            harpoon.add_file()

            local ok, notify = pcall(require, "notify") -- if notify is present add a notification

            if ok then
                local file_name = vim.fn.expand("%:p")
                local message = "Add file to harpoon: " .. file_name
                notify(message, "info", {
                    title = "Harpoon", }
                )
            end
        end

        vim.keymap.set('n', '<leader>;a', add_file, { desc = "Add buffer to harpoon" })
        vim.keymap.set('n', '<leader>tt', ':lua require("harpoon.ui").toggle_quick_menu()<CR>',
            { desc = "Toggle harpoon menu" })

        vim.keymap.set('n', '<leader>;&', ':lua require("harpoon.ui").nav_file(1)<CR>',
            { desc = "Move to harpoon [1] file" })

        vim.keymap.set('n', '<leader>;é', ':lua require("harpoon.ui").nav_file(2)<CR>',
            { desc = "Move to harpoon [2] file" })

        vim.keymap.set('n', '<leader>;"', ':lua require("harpoon.ui").nav_file(3)<CR>',
            { desc = "Move to harpoon [3] file" })

        vim.keymap.set('n', "<leader>;'", ':lua require("harpoon.ui").nav_file(4)<CR>',
            { desc = "Move to harpoon [4] file" })

        require("harpoon").setup({
            menu = {
                width = vim.api.nvim_win_get_width(0) - 20,
            }
        })
    end,
}
