return {
    -- Spec Source
    "Civitasv/cmake-tools.nvim",
    -- Spec Loading
    -- Spec Setup
    init = function()
        local loaded = false
        local function check()
            local cwd = vim.uv.cwd()

            if vim.fn.filereadable(cwd .. "/CMakeLists.txt") == 1 then
                require("lazy").load({ plugins = { "cmake-tools.nvim" } })
                loaded = true
            end
        end
        check()
        vim.api.nvim_create_autocmd("DirChanged", {
            callback = function()
                if not loaded then
                    check()
                end
            end,
        })
    end,
    config = function()
        require("cmake-tools").setup({
            cmake_build_directory = "build", -- this is used to specify generate directory for cmake, allows macro expansion, relative to vim.loop.cwd()
            cmake_executor = {               -- executor to use
                name = "overseer",           -- name of the executor
            },
            cmake_runner = {                 -- runner to use
                name = "overseer",           -- name of the runner
            }
        })

        vim.keymap.set("n", "<leader>cg", ":CMakeGenerate<CR>", {})
        vim.keymap.set("n", "<leader>cbt", ":CMakeSelectBuildTarget<CR>", {})
        vim.keymap.set("n", "<leader>cb", ":CMakeBuild<CR>", {})
        vim.keymap.set("n", "<leader>cr", ":CMakeRun<CR>", {})

        vim.api.nvim_create_autocmd("User", {
            pattern = "CMakeBuild",
            callback = function()
                vim.cmd('OverseerOpen') -- Open overseer window on CMake build event
            end,
        })
    end,
    lazy = true,
}
