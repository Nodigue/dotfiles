--- @param trunc_width number trunctates component when screen width is less then trunc_width
--- @param trunc_len number truncates component to trunc_len number of chars
--- @param hide_width number hides component when window width is smaller then hide_width
--- @param no_ellipsis boolean whether to disable adding '...' at end after truncation
--- return function that can format the component accordingly
local function trunc(trunc_width, trunc_len, hide_width, no_ellipsis)
    return function(str)
        local win_width = vim.fn.winwidth(0)
        if hide_width and win_width < hide_width then
            return ''
        elseif trunc_width and trunc_len and win_width < trunc_width and #str > trunc_len then
            return str:sub(1, trunc_len) .. (no_ellipsis and '' or '...')
        end
        return str
    end
end

return {
    'nvim-lualine/lualine.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    config = function()
        local lualine = require("lualine")
        local cmake = require("cmake-tools")
        local ros2 = require("ros2")

        lualine.setup {
            options = {
                icons_enabled = true,
                theme = 'gruvbox',
                component_separators = { left = '', right = '' },
                section_separators = { left = '', right = '' },
                disabled_filetypes = {
                    statusline = {},
                    winbar = {},
                },
                ignore_focus = {},
                always_divide_middle = true,
                globalstatus = false,
                refresh = {
                    statusline = 1000,
                    tabline = 1000,
                    winbar = 1000,
                }
            },

            -- Settings for the active buffer
            sections = {
                lualine_a = { 'mode' },
                lualine_b = {
                    {
                        'branch',
                        fmt = trunc(120, 20, 60, true),
                    },
                    {
                        'diff',
                        fmt = trunc(120, 20, 60, true),
                    },
                    {
                        'diagnostics',
                        fmt = trunc(120, 20, 60, true),
                    },
                    {
                        function()
                            if (not cmake.is_cmake_project()) then
                                return ""
                            end
                            local configure_preset = cmake.get_configure_preset()
                            local build_target = cmake.get_build_target()
                            local launch_target = cmake.get_launch_target()

                            return "Configure: " ..
                                (configure_preset or "X") ..
                                " | Build: " .. (build_target or "X") .. " | Launch: " .. (launch_target or "X")
                        end,
                        fmt = trunc(120, 20, 60, true),
                    },
                    {
                        function()
                            if ros2.is_workspace_candidate() then
                                return "ROS2"
                            else
                                return ""
                            end
                        end,
                        fmt = trunc(120, 20, 60, true),
                    }
                },
                lualine_c = { 'filename' },
                lualine_x = { 'encoding', 'fileformat', 'filetype' },
                lualine_y = { 'progress' },
                lualine_z = { 'location' }
            },

            -- Settings for the non-active buffers
            inactive_sections = {
                lualine_a = {},
                lualine_b = {},
                lualine_c = { 'filename' },
                lualine_x = {},
                lualine_y = {},
                lualine_z = {}
            },
            tabline = {},
            winbar = {},
            inactive_winbar = {},
            extensions = {}
        }
    end
}
