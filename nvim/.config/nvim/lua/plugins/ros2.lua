return {
    -- Spec Source

    -- A directory pointing to a local plugin
    dir = "/home/nicolas/docs/projects/ros2.nvim/",
    -- A custom name for the plugin used for the local plugin directory as the display name
    name = "ros2",
    -- When `true`, a local plugin directory will be used instead
    dev = true,

    -- Spec Loading

    -- Spec Setup
    -- `init` functions are always executed during startup
    init = function()
        local loaded = false
        local function check()
            local cwd = vim.uv.cwd()

            if vim.fn.filereadable(cwd .. "/.ros_workspace") == 1 then
                require("lazy").load({ plugins = { "ros2" } })
                loaded = true
            end
        end

        check()

        vim.api.nvim_create_autocmd("DirChanged", {
            callback = function()
                if not loaded then
                    check()
                end
            end,
        })
    end,
    -- `config` is executed when the plugin loads.
    config = true,

    -- Spec Lazy Loading

    -- When `true`, the plugin will only be loaded when needed
    lazy = true,
}
