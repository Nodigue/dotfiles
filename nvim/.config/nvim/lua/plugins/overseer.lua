return {
    -- Tasks runner
    {
        'stevearc/overseer.nvim',
        opts = {
            task_list = {
                bindings = {
                    ["?"] = "ShowHelp",
                    ["g?"] = "ShowHelp",
                    ["<CR>"] = "RunAction",
                    ["<C-e>"] = "Edit",
                    ["o"] = "Open",
                    ["<C-v>"] = "OpenVsplit",
                    ["<C-s>"] = "OpenSplit",
                    ["<C-f>"] = "OpenFloat",
                    ["<C-q>"] = "OpenQuickFix",
                    ["p"] = "TogglePreview",
                    ["<C-l>"] = false,
                    ["<C-h>"] = false,
                    ["L"] = false,
                    ["H"] = false,
                    ["["] = "DecreaseWidth",
                    ["]"] = "IncreaseWidth",
                    ["{"] = "PrevTask",
                    ["}"] = "NextTask",
                    ["<C-k>"] = false,
                    ["<C-j>"] = false,
                    ["q"] = "Close",
                },
            },
        },
        lazy = true,
        keys = {
            { "<leader>ot", ":OverseerToggle<CR>", desc = "Toggle Overseer", expr = false },
        },
    },
}
