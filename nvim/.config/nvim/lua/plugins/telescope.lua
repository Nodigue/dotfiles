return {
    "nvim-telescope/telescope.nvim",
    tag = "0.1.8",
    dependencies = {
        'nvim-lua/plenary.nvim',
        { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' }
    },
    config = function()
        local telescope = require("telescope")
        local builtin = require("telescope.builtin")
        local themes = require("telescope.themes")

        telescope.setup({
            pickers = {
                find_files = {
                    hidden = true,
                }
            },
            extensions = {
                fzf = {},
            }
        })

        telescope.load_extension('fzf')

        -- Open the help
        vim.keymap.set("n", "<leader>hh", builtin.help_tags, { desc = "Show help" })

        vim.keymap.set("n", "<C-p>", builtin.find_files, { desc = "Find file in the current directory" })
        vim.keymap.set("n", "<leader>gg", builtin.live_grep, { desc = "Search for a string" })

        -- Search in neovim config file
        vim.keymap.set("n", "<leader>nn", function()
            local opts = themes.get_ivy({
                -- cwd = vim.fn.stdpath("config") > Not working since we are doing hyperlinks
                cwd = vim.fn.expand("~/dotfiles/nvim/.config/nvim")
            })
            builtin.find_files(opts)
        end, { desc = "Show all nvim configuration files" })

        -- LSP
        vim.keymap.set("n", "<leader>rr", builtin.lsp_references, { desc = "Show all LSP references" })
        vim.keymap.set("n", "<leader>dd", builtin.lsp_definitions, { desc = "Show all LSP definitions" })

        -- Checkout git branches
        vim.keymap.set("n", "<leader>bb", function()
            local opts = themes.get_ivy()
            builtin.git_branches(opts)
        end, { desc = "Show all git branches" })
    end
}
