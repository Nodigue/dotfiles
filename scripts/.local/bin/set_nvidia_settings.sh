#!/usr/bin/env bash

# This scripts is run at startup and set the nivida settings for my monitor

# This is a workaround for a bug present on Ubuntu 20.04 where the nvidia
# settings are reset after a reboot

# Section "Screen"
#     Identifier     "Screen0"
#     Device         "Device0"
#     Monitor        "Monitor0"
#     DefaultDepth    24
#     Option         "Stereo" "0"
#     Option         "nvidiaXineramaInfoOrder" "DFP-1"
#     Option         "metamodes" "2560x1440_165 +0+0"
#     Option         "SLI" "Off"
#     Option         "MultiGPU" "Off"
#     Option         "BaseMosaic" "off"
#     SubSection     "Display"
#         Depth       24
#     EndSubSection
# EndSection

sh -c 'nvidia-settings --assign CurrentMetaMode="DFP-1: 2560x1440_165 +0+0"' > /dev/null 2>&1