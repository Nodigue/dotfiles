#!/bin/bash
#  ____                               _           _
# / ___|  ___ _ __ ___  ___ _ __  ___| |__   ___ | |_
# \___ \ / __| '__/ _ \/ _ \ '_ \/ __| '_ \ / _ \| __|
#  ___) | (__| | |  __/  __/ | | \__ \ | | | (_) | |_
# |____/ \___|_|  \___|\___|_| |_|___/_| |_|\___/ \__|
#
#
# Nicolas Raphanaud (Nodigue)
# -----------------------------------------------------

sleep 0.1

DIR="${HOME}/docs/images/screenshots/"
NAME="screenshot_$(date +%d%m%Y_%H%M%S).png"
FILE="${DIR}${NAME}"

case "$1" in
  "--fullscreen")
      scrot ${FILE}
      ;;
  "--area")
      scrot ${FILE} -s
      ;;
esac

if [[ -f ${FILE} ]]; then
    notify-send "📸 Screenshot" "${FILE}" -i ${FILE}
    xclip -selection clipboard -t image/png -i ${FILE}
fi


