#!/bin/sh

# Open rofi to prompt the song url link

choice=$(rofi -dmenu -p "🎵 Download song" -i)
# artist=$(rofi -dmenu -p "🎵 Artist" -i)
# genre=$(rofi -dmenu -p "🎵 Genre" -i)

cd ${HOME}/docs

output=$(ytdl-sub -l "verbose" -c ${XDG_CONFIG_HOME}/ytdl-sub/config.yaml dl \
                  --preset "single" \
                  --download.url "${choice}")

# output=$(ytdl-sub -l "verbose" -c ${XDG_CONFIG_HOME}/ytdl-sub/config.yaml dl \
#                   --preset "soundcloud_discography" \
#                   --overrides.sc_artist_url "${choice}"\
#                   --overrides.track_artist "${artist}"\
#                   --overrides.track_genre "${genre}")

echo ${output}
