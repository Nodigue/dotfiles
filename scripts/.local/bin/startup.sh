#!/bin/bash

# Load .Xresources
[ -f ~/.Xresources ] && xrdb -load $HOME/.Xresources

# Set keyboard layout to fr
setxkbmap fr

# CAPS Lock -> ESC
setxkbmap -option caps:escape

# Set Wallpaper
exec ${XDG_CONFIG_HOME}/wal/wallpaper.sh
