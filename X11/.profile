# ------------------------------------------------------------------------------------------- #
# Environnement Variables
# ------------------------------------------------------------------------------------------- #

export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority

export WGETRC="$XDG_CONFIG_HOME/wgetrc"

# NVIDIA
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv

# GTK
export GTK_RC_FILES="$XDG_CONFIG_HOME"/gtk-1.0/gtkrc
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc

# Python
export PYLINTHOME="$XDG_DATA_HOME/pylint"

# RUST
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup

# RUBY
export GEM_HOME="$XDG_DATA_HOME"/gem
export GEM_SPEC_CACHE="$XDG_CACHE_HOME"/gem

# ------------------------------------------------------------------------------------------- #
# Configuration
# ------------------------------------------------------------------------------------------- #

export DOTFILES="${HOME}/dotfiles"
source ${DOTFILES}/configuration.sh

# ------------------------------------------------------------------------------------------- #
# Path
# ------------------------------------------------------------------------------------------- #

export PATH="/usr/local/bin:$PATH"

if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# ------------------------------------------------------------------------------------------- #
# Bash
# ------------------------------------------------------------------------------------------- #

if [ -f "$HOME/.bashrc" ]; then
    source "$HOME/.bashrc"
fi

# ------------------------------------------------------------------------------------------- #
# StartX
# ------------------------------------------------------------------------------------------- #
if [[ -z "$DISPLAY" ]] && [[ $(tty) = /dev/tty1 ]]; then
    . startx
    logout
fi
