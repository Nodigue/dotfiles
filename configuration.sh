#!/bin/bash

export TERMINAL="alacritty"
export TERM="alacritty"
export EDITOR="nvim"
export BROWSER="firefox"
