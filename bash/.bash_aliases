# Linux
alias rp="source ${HOME}/.profile"
alias rb="source ${HOME}/.bashrc"

alias ll="ls -l"
alias la="ls -a"
alias lla="ls -la"

# Fonts
alias reload_fonts="sudo fc-cache -fv"

alias wget="wget --hsts-file="$XDG_CACHE_HOME/wget-hsts""

# Stow
function sstow() { stow -S --no-folding --adopt -vv $1; }
function sstowAll() { stow -S --no-folding --adopt -vv *; }
function ustow() { stow -D -vv $1; }
function ustowAll() { stow -D -vv *; }
function rstow() { ustow $1; sstow $1; }
function rstowAll() { ustowAll; sstowAll; }

function ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Text Editor
alias v="nvim"
alias vim="nvim"
